﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;

namespace TestWinFormsApp
{
    public partial class Form1 : Form
    {
        private static Logger logger;
        public Form1()
        {
            InitializeComponent();
            logger = LogManager.GetCurrentClassLogger();
        }

        private void num_button_Click(object sender, EventArgs e)
        {
            logger.Trace("Begin");
            Button num_button = (Button)sender;
            label1.Text = num_button.Text;
            logger.Trace("End");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int x_offset = 0;
            int y_offset = 10;

            for (int i = 0; i < 10; i++)
            {
                Button num_button = new Button();
                //Format controls. Note: Controls inherit color from parent form.
                num_button.BackColor = Color.Gray;
                num_button.Text = i.ToString();
                num_button.Size = new System.Drawing.Size(50, 25);
                num_button.Click += num_button_Click;

                this.Controls.Add(num_button);
                if (i % 3 == 0)
                {
                    x_offset = 0;
                    y_offset += 30;
                }
                
                num_button.Location = new System.Drawing.Point(10 + x_offset, y_offset);               
                x_offset += 50;
            }
        }
    }
}
